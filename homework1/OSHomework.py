import os


print("pid is:" + str(os.getpid()))
newpid = os.fork()

print(os.ctermid())

print("first child's pid is " + str(newpid))

if newpid == 0: 
	print("first child is dead. ")
	

newpid2 = os.fork()

print("second child's pid is: "+str(newpid2))

if newpid2 == 0: print("second child is dead. ")


